# Angluin Learning Algorithm

## Project Description

This project investigates how symbolic finite automata may be used to learn a defined language. 

A **.yml** file is used to render a gitlab page. This page displays a project report written by myself as well as a video  
explaining each section of code (sorry if I sound a little sleep-deprived, making this deadline was a wild ride).  
Additionally, the project specifications were included.  

The concept of symbolic finite automata was explored using the Angluin Learning Algorithm. The automata in this project  
process boolean algebra over a finite domain of unicode values. The algorithm is used in an attempt to learn a  
language defined by regular expressions. The specifics of these are discussed in the report as well as which  
functions were implemented and how they were implemented.

## Repository Description

The repository includes a **.yml** file. This was used to configure a gitlab page using Jekyll to generate a static  
site. The site is basic and written in vanilla html and css with some flexbox commands to make things easier.  
I did this for no particular reason, just wanted to mess around with pages and thought it was cooler than putting  
the link to a YouTube video in a readme.

The pdfs and background images are included in the **resources** folder, while the stylesheet is in the **css** folder.  
The **index.html** file is used to render the static page.

## To Run

The page is hosted on:  
https://8bitathena.gitlab.io/angluin-learning-algorithm

You may also download the project file and double-click on the **index.html** file.

